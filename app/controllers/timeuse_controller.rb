class TimeuseController < ApplicationController
  def index
    @participants = Participant.where(:delete_flag => 0)
  end

  def details
    @hours = create_hours(:start_time => 3.hours, :end_time => (26.75).hours)
    @main_activities = MainActivity.where(:delete_flag => 0)
    @sub_activities = SubActivity.where(:delete_flag => 0)
  end

  def create_hours(parameters)
    start_time = parameters[:start_time] ? parameters[:start_time] : 0
    end_time = parameters[:end_time] ? parameters[:end_time] : 24.hours
    increment = parameters[:increment] ? parameters[:increment] : 15.minutes

    Array.new(1 + (end_time - start_time)/increment) do |i|
      (Time.now.midnight + (i*increment) + start_time).strftime("%I:%M %p")
    end
  end
end
