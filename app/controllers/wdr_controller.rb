#coding:utf-8
require 'csv'

class WdrController < ApplicationController
  def index
    @wdr = Wdr.where(:delete_flag => 0)
  end

  def keywords
    @keywords = Keywords.where(:delete_flag => 0)
    @k_list = @keywords.map {|h| h[:keyword]}.uniq.sort
    @years = (1978..2015).to_a

    unless params.blank?
      cond = {}
      if params[:word].present?
	cond[:keyword] = params[:word]
      end

      if params[:year].present?
	cond[:year] = params[:year]
      end

      @keywords = Keywords.where(cond, :delete_flag => 0)
    end
  end

  def keywords_summary
    @keywords = Keywords.where(:delete_flag => 0)

    @top_keywords = {}

    uniq_keywords = {}
    @keywords.each do | k |
      unless uniq_keywords[k[:keyword]].nil?
	uniq_keywords[k[:keyword]][:count] += 1
	uniq_keywords[k[:keyword]][:total_f] += k[:count]
	uniq_keywords[k[:keyword]][:years] << "#{k[:year]}"
      else
	uniq_keywords[k[:keyword]] = {
	  :count => 1, 
	  :years => ["#{k[:year]}"],
	  :total_f => k[:count]
	}
      end

      if @top_keywords[k[:year]].nil?
	@top_keywords[k[:year]] = {
	  :keyword => k[:keyword],
	  :count => k[:count]
	}
      end
    end

    @top_keywords = Hash[@top_keywords.sort_by {|k,v| v[:count]}.reverse]

    @k_summary = {}
    uniq_keywords.each do | k |
      if k[1][:total_f].to_i > 1000
	@k_summary[k[0]] = k[1]
      end

    end

    @k_summary = Hash[uniq_keywords.sort_by {|k,v| v[:count]}.reverse]
    @k_summary = uniq_keywords
  end

  def image
    years = (1991..1994).to_a

    @items = {}
    years.each do | year |
      next if year == 1999
      path = "#{Rails.root}/public/wdr_images/#{year}/"

      logger.debug "----#{year}----"
      @items[year] = []
      Dir.foreach(path) do | item |
	next if item == '.' or item == '..'
	@items[year] << item
      end
      logger.debug @items[year]
    end
  end

  def stats
    @words = {}
    @sum = {}
    @colors = []
    @uniq_words = []
    
    @csv = CSV.open("#{Rails.root}/docs/chapters/results/wdr_chapcount_top5.csv", "r:ASCII-8BIT")

    @csv.each do | c |
      if @words[c[0]].present?
	@words[c[0]] << {:word => c[1],:count => c[2].to_i}
	@sum[c[0]] += c[2].to_i
	@uniq_words << c[1]
      else
	@sum[c[0]] = c[2].to_i
	@words[c[0]] = [] 
	@words[c[0]] << {:word => c[1],:count => c[2].to_i}
	@uniq_words << c[1]
      end
    end

    @uniq_words.uniq!

    u_words = @uniq_words
    data = @words

    @keywords = {}
    u_words.each do | w |
      @keywords[w] = []
    end

    data.each do | year, d |
      d.each do | k |
	@keywords[k[:word]] << {year.to_i => k[:count]}
      end
    end
  end
end
