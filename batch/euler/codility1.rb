# you can write to stdout for debugging purposes, e.g.
# puts "this is a debug message"

def solution(a)
    # write your code in Ruby 2.2
    return -1 if a == 0

    sum = 0
    a.each do | arr |
        sum += arr
    end
    
    lsum = 0
    a.each_with_index do | arr, i |
        rsum = sum - lsum - arr
        return i if (lsum == rsum) 
        lsum += arr
    end
    
    return -1
end