#What is the largest prime factor of the number 600851475143?

num = 600851475143
prod = 1

for i in 2..num
	if num % i == 0
		puts i
		prod *= i
	end

	break if prod == num
end