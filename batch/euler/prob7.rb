primes = [2]
count = 1
n = 10001

i = 3
begin
	is_prime = true
	primes.each do |pr|
		if i % pr == 0
			is_prime = false
			break
		end
	end

	if is_prime
		primes << i
		count += 1
	end

	i += 1
end while count < n

p primes.last