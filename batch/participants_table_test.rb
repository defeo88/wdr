str1 = "M is a company blah blah blah in 2003 with Internet something Connected to M.
Furthermore it was M estabilished in a Good time and it means Medicine, Medical, Money."

str2 = "Japanese, English, Science, Mathematics
78, 99, 80, 85
82, 84, 88, 90
92, 93, 91, 80
86, 78, 71, 76"

#count words that starts in capital letters and number. Repeated words are considered as 1 entry.
regex1 = /^[A-Z]|^[0-9]/
lines = str1.split("\n")

uppercase = []
lines.each do | line |
  words = line.split(" ")
  words.each do | word |
    uppercase << word.tr(',.','').strip if word.match(regex1)
  end
end

#compute for the average of each subject.
lines2 = str2.split("\n")
line_count = lines2.count
sum = Array.new(line_count - 1, 0)
subjects = []

lines2.each_with_index do | line2, i |
  next if i == 0
  words2 = line2.split(",")
  words2.each_with_index do | word2, j |
    sum[j] += word2.strip.to_i
  end
end

avg = []
sum.each do | s |
  avg << s / (line_count - 1).to_f
end

puts lines2[0]
puts avg.join(",")
