=begin
n = STDIN.gets.chomp.to_i
bin = n.to_s(2)
count_one = bin.count('1')
if count_one == 1
	puts 'YES'
else
	puts 'NO'
end
=end

=begin
def get_ave(str)
	words = str.split(" ").map {|x| x.length}
	return words.inject(0.0) {|sum, el| sum + el} / words.count
end

#strs = ["This is a dog.", "{This,That} is a {cat,dog,bear}.", "{a,aa,aaaaaa} bbbbb {c,ddddd,eee}."]

str = "{This,That} is a {cat,dog,bear}."
#str = "This is a dog."

str.gsub!(".", "")
temp1 = str.scan(/\{(.*?)\}/)
temp2 = str.match(/\}(.*?)\{/)

if temp1.empty?
	print(get_ave(str))
else
	words1 = temp1[0][0].split(",")
	words2 = temp1[1][0].split(",")

	ave = []
	words1.each do |w1|
		words2.each do |w2|
			ave << get_ave("#{w1} #{temp2[0].delete("}{").strip} #{w2}")
		end
	end
	print(ave.max)
end
=end

class Numeric
  Alph = ("a".."z").to_a
  def alph
    s, q = "", self
    (q, r = (q - 1).divmod(26)); s.prepend(Alph[r]) until q.zero?
    s
  end
end

str = "abc+def^3-4"
#str = "3+4*9"

p str.alph

if str.include?("^")
	arr1 = str.split("^")
	p arr1
end

exp = str.split("^")
add = str.split("+")
minus = str.split("-")



=begin
str = "6 5
1 2
3 2
5 1
1 5
2 4
"
lines = str.split("\n")

lines.each do | line |
	num = line.split(" ")

end
=end
