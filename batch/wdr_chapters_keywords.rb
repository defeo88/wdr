require 'csv'

ng_words = File.readlines("docs/chapters/wdr_non_keywords.txt")
ng_words.map{|x| x.strip! }

years = (1979..2015).to_a

years.each do | year |
  next if year == 1999
  f = File.open("docs/figure_titles/figure_title_#{year}.txt")
  contents = f.readlines
  p "----#{year} ok----"
  p contents
  contents.each do | content |
	  contents = contents.downcase.strip.split(" ")
  figure_title = contents[0]
  end
  
  
  p figure_title
  break
  contents.delete_if{|word| word.match(/\d/){true}} #remove numbers
  contents.reject!{|word| ng_words.include?(word)} #remove junk words
  
  contents.map{|word| word.delete!(":|,|.|*|&|%|^|?|\'|\"|(|)")} #remove junk symbols
  
  #count occurring keywords
  keyword = {}
  contents.each do | word |
	  unless keyword[word].nil?
		  keyword[word] += 1
	  else
		  keyword[word] = 1
	  end
  end

  #sort keyword hash contents
  #symbols = "(), , w, dc, ii, iv, all, fi, in, countries, rep, po, no, ltd, see, (isbn".split(", ")
  #alphabet = ('a'..'z').to_a
  #new_ng_words = symbols + alphabet

  keyword.reject!{|word| ng_words.include?(word)} #remove junk words
  keyword = Hash[keyword.sort_by { | key, val | val }.reverse]
  word_list = keyword

  p word_list

  CSV.open("docs/figure_titles/wdr_figure_titles_top2.csv", "a+") do | csv |
	  word_list.each do | word |
		  csv << [year] + word
	  end
  end
end
