require 'csv'

path = "public/wdr_images/wdr_color_info.csv"

year = ""
@color_data = {}
CSV.foreach(path) do | row |
  unless row[0].nil?
	  year = "#{row[0]}"
	  p "-----"
	  p year
  end
  
  color_info = row[2].split("%")
  color_name = color_info[1].strip
  color_name_basic = color_name.scan(/\(([^\)]+)\)/).first[0]

  #0 - img_no, 1 - hex, 2 - area
  color_info = color_info[0].split(" ").insert(0, row[1])
  
  CSV.open("public/wdr_images/wdr_color_data_summary.csv", "a") do | csv |
	  csv << [year, color_info[0], color_info[1], color_info[2], color_name, color_name_basic]
  end
  p color_info

=begin
  if @color_data[year].nil?
	  @color_data[year] = color_info
  else
	  @color_data[year] << color_info
  end
=end
end
