require 'csv'

uniq_keywords = {}
year = 1978

puts "----#{year}----"

i = 0
CSV.foreach("docs/figure_titles/wdr_figure_titles_top2.csv") do | k |
  unless uniq_keywords[k[1]].nil?
	  uniq_keywords[k[1]][:total_f] += k[2].to_i
	  uniq_keywords[k[1]][:year] << k[0]
	  uniq_keywords[k[1]][:titles] << k[3]
  else
	  uniq_keywords[k[1]] = {
		  :total_f => k[2].to_i,
		  :year => [k[0]],
		  :titles => [k[3]]
	  }
  end

  if k[0].to_i != year
	  year += 1
	  #puts "----#{year}----"
  end
end

junk_words = "across, real, high".split(", ")
uniq_keywords.reject!{|word| junk_words.include?(word)} #remove junk words
keywords = Hash[uniq_keywords.sort_by {|k,v| v[:total_f]}.reverse].first(50)

#p keywords
figure_titles = []
keywords.each do | k |
  #puts "years: #{k[1][:year].first}"
  if k[0].match(/women|female|poor|poverty|health|mortality/) 
	  titles = k[1][:titles].join(", ")
	  t = titles.split(", ")
	  figure_titles << t
	  
	  puts "------------------------"
	  puts "#{k[1][:year].first} \t #{k[1][:total_f]} \t #{k[0]}"
	  puts "------------------------"
	  #puts "#{titles}"
  end
end

figure_titles = figure_titles.join(", ").split(", ")
uniq_titles = figure_titles.uniq

titles_hash = {}
uniq_titles.each do | t |
  keywords.each do | k |
	  if k[0].match(/women|female|poor|poverty|health|mortality/) 
		  titles = k[1][:titles].join(", ").split(", ")
		  
		  if titles.include?(t)
			  unless titles_hash[t].nil?
				  titles_hash[t][:keywords] << k[0]
			  else
				  titles_hash[t] = {
					  :keywords => [k[0]]
				  }
			  end
		  end
	  end
  end
end

#p titles_hash

CSV.open("docs/figure_titles/wdr_figure_keywords.csv", "a+") do | csv |
  titles_hash.each do | title, val |
	  csv << [title, val[:keywords].join(", ")]
  end
end
