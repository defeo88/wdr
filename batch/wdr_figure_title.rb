years = (2000..2015).to_a
save_file = open("docs/wdr_figure_title_results.txt", "w")


years.each do | year |
  next if year == 1999
  f = File.open("docs/figure_titles/figure_title_#{year}.txt")
  contents = f.readlines

  p "----#{year}-----"
  results = {}
  contents.each do | word |
	  sections = word.split(" ")

	  chapter = sections[0].split(".").first
	  page_no = sections.last
	  
	  unless sections[0].include?(".")
		  chapter = "0_#{sections[0]}"
	  end
	  
	  unless results[chapter].nil?
		  results[chapter] += 1
	  else
		  results[chapter] = 1
	  end
  end
  puts results
end

save_file.close
