require 'csv'

ng_words = File.readlines("docs/chapters/wdr_non_keywords.txt")
ng_words.map{|x| x.strip! }

years = (1978..2015).to_a

years.each do | year |
  next if year == 1999
  f = File.open("docs/figure_titles/figure_title_#{year}.txt")
  contents = f.readlines
  p "----#{year} ok----"

  keyword = {}
  contents.each do | content |
	  content = content.downcase.strip.split(" ")
	  figure_title = "#{year}_#{content[0]}"

	  content.delete_if{|word| word.match(/\d/){true}} #remove numbers
	  content.reject!{|word| ng_words.include?(word)} #remove junk words	
	  content.map{|word| word.delete!(":|,|.|*|&|%|^|?|\'|\"|(|)")} #remove junk symbols

	  content.each do | word |
		  unless keyword[word].nil?
			  keyword[word][:f] += 1
			  keyword[word][:titles] << figure_title
		  else
			  keyword[word] = {:f => 1, :titles => [figure_title]}
		  end
	  end	
  end

  keyword.reject!{|word| ng_words.include?(word)} #remove junk words
  keyword = Hash[keyword.sort_by { | key, val | val[:f] }.reverse]
  word_list = keyword

  CSV.open("docs/figure_titles/wdr_figure_titles_top2.csv", "a+") do | csv |
	  word_list.each do | word, val |
		  csv << [year, word, val[:f], val[:titles].join(", ")]
	  end
  end
end
