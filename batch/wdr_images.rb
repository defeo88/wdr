require 'exifr'
require 'csv'

years = (1978..2015).to_a
temp = [2009,2010,2013]

img_width = []
img_height = []

years.each do | year |
  path = "public/wdr_images/#{year}/"
  
	  a_width = []
	  a_height = []

	  if temp.include?(year)
	  Dir.foreach(path) do | item |
		  next if item == '.' or item == '..'
		  width = EXIFR::JPEG.new(path + item).width
		  height = EXIFR::JPEG.new(path + item).height

		  img_width =  (width * 0.35) + width
		  img_height = (height * 0.35) + height
		  
		  puts "#{img_width.round(1)}\t#{img_height.round(1)}\t#{item}"


		  #a_width << img_width
		  #a_height << img_height
	  end
	  end

=begin
	  width_total = a_width.inject(:+)
	  height_total = a_height.inject(:+)
	  width_ave = width_total.to_f/a_width.length
	  height_ave = height_total.to_f/a_height.length

	  puts "--------#{year}------------"
	  puts "width min = #{a_width.min}, width max = #{a_width.max}"
	  puts "height min = #{a_height.min}, height max = #{a_height.max}"
	  puts "width ave = #{width_ave.round(0)}, height_ave = #{height_ave.round(0)}"
	  
	  CSV.open("public/wdr_images/wdr_image_data_summary_new.csv", "a") do | csv |
		  csv << [year, a_width.min, a_width.max, a_height.min, a_height.max, width_ave.round(0), height_ave.round(0)]
	  end	
=end

end
