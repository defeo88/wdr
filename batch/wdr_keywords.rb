years = (1978..2015).to_a
keywords = File.readlines("../docs/wdr_keywords.txt")

year_hash = {}
keyword_info = {}

years.each do | year |
  year_hash.merge!({year => {}})

  begin
	  f = File.open("../docs/wdr_#{year}e.txt")
	  contents = f.read	
  rescue
	  next
  end
	  
  keywords.each do | k |
	  k = k.downcase.strip
	  word_count = contents.downcase.scan(k).count
	  year_hash[year].merge!({k => word_count})
  end	

  key_sorted = Hash[year_hash[year].sort_by { | key, val | val }.reverse]
  keyword_info.merge!({year => key_sorted})
  
  f.close
end

p keyword_info.inspect
#p keyword_info

File.open("../docs/wdr_keywords_summary.txt", "w+") do |f_key|
  f_key.write(keyword_info)
end

=begin
  #rewriting contents
  contents = f.read.gsub("\n", ' ').squeeze(' ')
  File.open("../docs/wdr_#{year}e.txt", "w+") do |f_new|
	  f_new.write(contents)
  end
=end
