require 'csv'
keywords = [
#"women","poor","health","climate","growth",
#"population", "debt", "prices", "trade", "public", "financial", "environmental", 
#"oil", "infrastructure", "labor", "transition", "state", "knowledge", "institutions", 
"bank", "investment", "young", "agricultural", "economic"
]

results = {
  "women" 	=> {},
  "poor" 		=> {},
  "health"	=> {},
  "climate"	=> {},
  "growth"	=> {},
=begin	
  "oil"		=> {},
  "population" => {},
  "debt"		=> {},
  "prices"	=> {},
  "trade"	=> {},
  "public"	=> {},
  "financial"	=> {},
  "environmental"	=> {},
  "infrastructure"	=> {},
  "labor"	=> {},
  "transition"	=> {},
  "state"	=> {},
  "knowledge"	=> {},
  "institutions"	=> {},
  "bank"	=> {},
  "investment"	=> {},
  "young"	=> {},
  "agricultural"	=> {},
  "economic"	=> {},
  "violence"	=> {},
  "jobs"		=> {},
  "risk"		=> {},
  "social"	=> {},
=end
}

CSV.foreach("docs/report_text/results/wdr_keyword_allcount.csv") do | item |
  keywords.each do | k |
	  if item[1].match(k)
		  unless results[k][item[0]].nil?
			  results[k][item[0]] += item[2].to_i
		  else
			  results[k].merge!({item[0] => item[2].to_i})
		  end
	  end
  end
end


results.each do | res, val |
  p "--- #{res} ---"
  val.each do | k, v |
	  puts "#{k} \t #{v}"
  end

end
