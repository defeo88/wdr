require 'csv'

uniq_keywords = {}
year = 1978

puts "----#{year}----"

i = 0
CSV.foreach("docs/figure_titles/wdr_figure_titles_top.csv") do | k |
  #p k
  unless uniq_keywords[k[1]].nil?
	  uniq_keywords[k[1]][:total_f] += k[2].to_i
	  uniq_keywords[k[1]][:year] << k[0]
  else
	  uniq_keywords[k[1]] = {
		  :total_f => k[2].to_i,
		  :year => [k[0]]
	  }
  end

  if k[0].to_i != year
	  year += 1
	  puts "----#{year}----"
  end

  #i+=1
  #break if i == 6000
end

junk_words = "across, real, high".split(", ")
uniq_keywords.reject!{|word| junk_words.include?(word)} #remove junk words
keywords = Hash[uniq_keywords.sort_by {|k,v| v[:total_f]}.reverse].first(40)

p keywords

#keywords = [["growth", {:total_f=>13786}], ["economic", {:total_f=>13499}], ["health", {:total_f=>9361}], ["income", {:total_f=>8405}], ["social", {:total_f=>8071}], ["poor", {:total_f=>8058}], ["trade", {:total_f=>8006}], ["economies", {:total_f=>7860}], ["population", {:total_f=>7860}], ["market", {:total_f=>7133}], ["labor", {:total_f=>7071}], ["investment", {:total_f=>7058}], ["africa", {:total_f=>6777}], ["education", {:total_f=>6753}], ["poverty", {:total_f=>6551}], ["capital", {:total_f=>5784}], ["institutions", {:total_f=>5738}], ["women", {:total_f=>5720}], ["financial", {:total_f=>5685}], ["rural", {:total_f=>5630}]]
keywords.each do | k |
  #puts "years: #{k[1][:year].first}"
  puts "#{k[1][:year].first} \t #{k[1][:total_f]} \t #{k[0]}"
end
