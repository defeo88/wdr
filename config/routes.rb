Rails.application.routes.draw do
  #timeuse main
  match "/timeuse"       			  => "timeuse#index", :via => :get
  match "/timeuse/details/:id"	=> "timeuse#details", :via => :get

  #main activity
  match "/activity"				     => "activity#index", :via => :get
  match "/activity/edit/:id"	 => "activity#edit", :via => :get
  match "/activity/delete/:id" => "activity#delete", :via => :get

  #sub activity
  match "/subactivity"	=> "subactivity#index", :via => :get

  #wdr
  match "/wdr"		      => "wdr#index", :via => :get
  match "/wdr/stats"          => "wdr#stats", :via => :get
  match "/wdr/color"    => "wdr#color", :via => :get
  match "/wdr/image"    => "wdr#image", :via => :get
  match "/wdr/keywords"    => "wdr#keywords", :via => :get
  match "/wdr/keywords/summary"    => "wdr#keywords_summary", :via => :get

  #timer
  match "/timer"  => "timer#index", :via => :get
end
