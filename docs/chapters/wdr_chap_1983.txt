﻿The 1980-82 recession 1
International collaboration 3
National development efforts 4
Part I World Economic Recession and Prospects for Recovery 7
2 The prolonged recession 7
The delayed recovery 8
International trade 9
Capital flows 16
Financing deficits in the recession 18
International debt 20
The impact of the recession on developing countries 23
3 The outlook for developing countries 27
Growth in industrial countries 27
Energy 28
Trade 29
Workers' remittances 31
The resource gap 32
Implications of the Low case 34
The possibilities for faster growth 36
Domestic determinants of developing-country growth 37
Conclusions 39
Part II Management in Development 41
4 The search for efficiency 41
The analysis of efficiency 41
The potential for greater efficiency 43
The framework for improving efficiency 46
5 The role of the state 47
Public spending 47
The state as producer 48
The state as regulator 52
Conclusions 56
6 Pricing for efficiency 57
Price distortions 57
Linkages among distortions 59
Price distortions and growth 60
7 National economic management 64
Macroeconomic policies and adjustment 64
Economic management and planning 66
Managing in uncertain times 69
Improving links between planning, budgeting, and evaluation
8 Managing state-owned enterprises 74
The growing fiscal burden 74
The nature of SOEs 75
Defining objectives 76
Control without interference 78
Institutional links between government and enterprise 78
Holding managers accountable for results 81
Liquidation 85
Divestiture 85
Agenda for reform 86
9 Project and program management 88
Managing physical development 88
Managing people-centered development 92
Managing multiagency programs 96
Some lessons learned: a summary 99
10 Managing the public service 101
Availability and distribution of skills 101
Public service training 106
Personnel policies and management 109
A concluding note: the cultural dimension 113
11 Reorienting government 115
The historical and political context 115
Managing administrative change 116
Economizing on management 117
Making bureaucracies responsive 123
Conclusions 124
12 Concluding themes 125
1.1 GDP growth rates, 1965-82 2
2.1 Two periods of recession in industrial countries, 1974-75 and 1980-82 8
2.2 Petroleum prices, 1972-83 8
2.3 Interest rates, real and nominal, 1970-82 9
2.4 Export earnings of developing countries, 1965 and 1980 10
2.5 Composite commodity price index, 1948-82 11
2.6 World prices for selected commodities, 1960-82 11
2.7 Balance of payments financing of all developing countries, 1970 and 1982 16
2.8 Capital flows to developing countries, 1982 16
2.9 Current account financing of all developing countries, 1970-82 20
2.10 Debt and debt service ratio of all developing countries, 1970-82 20
2.11 Growth of debt and exports, 1970-83 21
3.1 Global energy consumption, 1970-95 29
3.2 Real GDP growth of industrial countries and export volume growth
of developing countries, 1966-82 29
3.3 Price index of selected commodities, 1982-95 32
3.4 Debt and exports of all developing countries, 1970-95 35
4.1 Growth, investment, and return on investment, 1960-70 and 1970-80 43
5.1 Central government revenue, 1960-80 47
5.2 Government expenditure by category, 1980 48
5.3 Central government expenditure by sector 49
5.4 State-owned enterprises' share of GDP by sector 50
5.5 State-owned enterprises' share of value added in manufacturing 51
5.6 Nonfinancial state-owned enterprises' share of GDP 51
6.1 Price distortions and growth in the 1970s 62
8.1 Net claims on the budget of nonfinancial state-owned enterprises 75
8.2 Growth of nonfinancial state-owned enterprises 76
9.1 Number of cities with populations of more than one million, 1960-2000 97
