﻿1 Industrialization and foreign trade: an overview
From recovery and adjustment to long-term growth 2
Industrialization: trends and transformations 5
The role of government 7
Trade policy and industrialization 8
Trade policy reform 8
Complementary policies for industrial development 9
The threat of protectionism 10
Toward a more open trading system 10
Part I Barriers to Adjustment and Growth in the World Economy
2 From recovery and adjustment to long-term growth 14
The weakening recovery and international payments imbalances 15
Policies for the short term 21
Policies for medium- and long-term growth 24
The long-term adjustment issues 27
Conclusion 35
Part II Industrialization and Foreign Trade
3 Industrialization: trends and transformations 38
Global industrialization in historical perspective 38
Global industrialization after World War II 43
Industrialization and structural change 48
Lessons from industrialization experiences before World War II 54
Conclusion 57
4 The role of government 58
Governments and industrialization 58
The direct role of government: public goods and public services 60
The indirect role of government: intervening in markets 68
Government policies and the high costs of doing business 72
The priorities for government 76
5 Trade policy and industrialization 78
Alternative trade strategies 78
Why outward orientation works 88
Trade strategy in perspective 92
6 Trade policy reform 95
The diversity of country experience 98
The transition to more outward-oriented policies 106
The design of trade policy reform 109
The lessons of trade liberalization 112
7 Complementary policies for industrial development 113
The policy choices 113
Factor prices 124
The competitive environment 127
Economic policy and technological development 129
Conclusion 131
8 The threat of protectionism 133
The rise and fall of trade liberalization 134
The increase in protectionist measures 139
Explanations for the growth of protectionism 140
Has protectionism retarded trade? 147
Net costs to developing countries of industrial countries' protection 148
Net costs to industrial countries of their own protection 150
Conclusion 153
9 Toward a more open trading system 154
Problems with the trading system 155
The attraction of nontariff barriers 157
Targets for reform 157
The stake of the developing countries in the Uruguay Round 166
10 Industrialization and the world economy: a policy agenda 168
A policy agenda for industrial countries 168
A policy agenda for developing countries 169
The international environment for trade and finance 169
1.1 Long-term trends in GDP and employment by sector, selected years, 1890-1984 6
2.1 Real GDP growth, 1973-86 15
2.2 Inflation, 1973-86 16
2.3 Real non-oil commodity prices, 1950-86 17
2.4 Interest rates, 1980-86 19
2.5 The dollar exchange rate, 1979-86 24
3.1 Global manufacturing: the component network for the Ford Escort (Europe) 39
3.2 Historical trends in the growth of real GDP and exports in selected countries, 1720-1985 40
3.3 Postwar growth in world output and exports 43
3.4 Share of offshore assembly products in total manufactured imports by the United States from selected developing economies, 1973-85 45
3.5 Indicators of industrial performance of developing economies 49
3.6 Historical relationship between GDP per capita and the share of industry in GDP in selected industrial countries, 1870-1984 50
3.7 Relationship between GDP per capita and the share of manufacturing value added in GDP in selected economies, 1984 51
4.1 Educational profile of the labor force in selected developing countries 63
4.2 Economic development and industrial infrastructure 65
4.3 Economic development and government expenditure on transport and communications 66
4.4 Public ownership of selected industrial sectors, 1984 67
5.1 Classification of forty-one developing economies by trade orientation, 1963-73 and 1973-85 83
5.2 Macroeconomic performance of forty-one developing economies grouped by trade orientation 84
5.3 Economic and industrial performance by trade orientation 86
6.1 Major economic liberalization policies in the Republic of Korea, 1959-83 99
6.2 Evolution of trade incentives in Korea, 1958-84 100
6.3 Major economic liberalization policies in Chile, 1972-86 102
6.4 Real exchange rate, imports, and exports in Chile, 1960-86 103
7.1 The stock of foreign direct investment in developing economies 117
7.2 Financial savings and the real deposit rate in selected developing countries 119
7.3 The share of the public sector in total employment in selected developing countries 124
8.1 The incidence of export restraint arrangements on manufactures, 1984-86 140
8.2 Share of manufactures imported by the OECD from Japan and NICs, 1965-85 143
8.3 World exports of manufactures, 1963 and 1985 147
8.4 The cost to consumers of preserving a job in selected industries, 1983 152
