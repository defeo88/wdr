﻿Public finance in development: an overview 1
Policy options for global adjustment 2
The role of public finance in development 5
Fiscal policy for stabilization and adjustment 6
Reforming tax systems 7
Improving the allocation of public spending 7
Spending priorities and revenue options in selected sectors 8
Financing local government 9
Strengthening public finance through reform of state-owned enterprises 10
Directions for reform 10
Part I Opportunities and Risks in Managing the World Economy
1 Policy options for global adjustment 13
The legacy of the 1970s 13
Macroeconomic policies and imbalances in industrial countries 15
Developing countries in the world economy 23
The outlook for the world economy until 1995 35
Part II Public Finance in Development
2 The role of public finance in development 43
Patterns of public finance 44
Evolving views of the public sector 48
A pragmatic approach to public policy 51
3 Fiscal policy for stabilization and adjustment 55
Fiscal policy and macroeconomic performance 55
The fiscal dimension of the external debt crisis 64
The fiscal management of commodity export cycles 71
Adjustment in low-income Sub-Saharan Africa 74
Fiscal policy and the growth imperative 78
4 Reforming tax systems 79
Patterns of taxation 80
Objectives and constraints in tax reform 83
Commodity taxation 86
Income taxes 92
Tax administration 99
The scope for tax reform 101
5 Improving the allocation of public spending 105
Patterns and trends in government spending 105
Priorities for public spending 112
Planning and budgeting public spending 120
6 Spending priorities and revenue options in selected sectors 131
Education and health 131
Urban services 142
Rural infrastructure 148
7 Financing local government 154
Patterns of subnational government finance 154
Fiscal decentralization and the role of subnational government 157
Strengthening local government finance 158
Toward more efficient local government 167
8 Strengthening public finance through reform of state-owned enterprises 168
How SOEs interact with public finances 168
Strengthening SOEs through fiscal instruments 173
Enhancing fiscal discipline 175
Reappraising the environment and the scope of SOEs 177
Agenda for SOE reform 180
9 Directions for reform 182
Prudent budget policies 182
Reduced costs of raising revenue 183
Efficient and effective public spending 183
Strengthened autonomy and accountability of decentralized public entities 184
Public finance policies consistent with poverty alleviation 185
1 Ratios of investment to GDP in developing countries, 1970 to 1986 2
2 Per capita GDP during the Great Depression and the current crisis in selected countries 3
1.1 Actual and projected growth of GDP, 1960 to 1990 14
1.2 Inflation, 1973 to 1987 15
1.3 Real interest rates in major industrial countries, 1979 to 1987 18
1.4 Current account balance in industrial countries, 1980 to 1987 19
1.5 Real effective exchange rates of key currencies, 1978 to 1987 20
1.6 Real commodity prices, 1970 to 1987 25
1.7 Volume and purchasing power of exports by developing regions, 1965 to 1987 26
1.8 Long-term external debt of developing countries, 1970 to 1987 29
1.9 Interest rates on external borrowings of developing countries, 1976 to 1987 29
1.10 Net resource transfers to developing countries, 1973 to 1987 30
2.1 Public sector deficits in selected developing countries, 1979 to 1985 44
2.2 The relation between per capita GNP and the share of central government expenditure in GNP, 1985 46
2.3 Total public sector expenditure as a share of GDP in selected developing countries, 1985 47
2.4 Growth of public debt and the composition of total external debt 48
2.5 Relation between central government expenditure as a share of GDP and the growth of GDP in developing countries 52
3.1 Public deficits and current account deficits in four countries, 1977 to 1986 60
3.2 Real effective exchange rate indexes for selected countries 62
3.3 Overall and primary public balances for four middle-income debtors, 1977 to 1985 63
3.4 Net transfers, current account deficits, and public deficits for seventeen highly indebted countries, 1981 to 1985 64
3.5 Aggregate production and expenditure in highly indebted and successfully adjusting countries, 1980 to 1986 70
3.6 Public revenues and expenditures during commodity booms 72
3.7 Net flows of medium- and long-term debt financing to Sub-Saharan Africa, 1980 to 1986 76
4.1 Share of tax and nontax revenue in central government current revenue, 1975 and 1985 80
4.2 Trends in ratios of tax revenues to GDP, 1975 to 1985 82
4.3 Variations in tax composition, by income group, 1975 and 1985 83
4.4 Variations in taxes, by regional group, 1985 84
4.5 Marginal economic costs of raising revenue from tariffs and domestic commodity taxes in the Philippines 85
4.6 Countries in which agricultural export taxes provide more than 5 percent of tax revenue for selected years 92
4.7 Asset-specific marginal effective tax rates in Malawi, 1974 and 1984 92
4.8 Income level at which personal income tax liability begins and the subsequent structure of the marginal tax rates during 1984 and 1985 96
4.9 Maximum marginal tax rate (MTR) and the level of personal income at which it becomes effective during 1984 and 1985 97
5.1 Central government spending as a share of GDP by region, 1975 to 1985 106
5.2 Allocation of central government spending by economic category, 1980 109
5.3 Shares of GDP allocated by the central government to various economic categories, 1980 109
5.4 Allocation of central government spending by functional category, 1980 110
5.5 Shares of GDP allocated by the central government to various functional categories, 1980 110
5.6 Trends in central government interest payments, 1975 to 1985 111
5.7 Central government spending per capita, 1975 and 1984 112
5.8 Real reduction in central government spending in fifteen countries, early 1980s 113
5.9 Growth in central government employment 115
6.1 Range of approximate cost of each additional life saved by various health services in developing countries 134
6.2 Cost per student of different levels of public education as a percentage of GNP per capita in three country groups, early 1980s 135
7.1 Size of subnational and local governments, averages for 1974 to 1986 155
7.2 Spending and revenue of subnational governments, averages for 1974 to 1986 156
7.3 Fiscal decentralization to the subnational level, averages for 1974 to 1986 157
8.1 Nonfinancial SOE shares of value added and investment 169
8.2 Average annual net transfers from government to nonfinancial SOEs 170
8.3 SOE contributions to the growth of external debt in developing countries, 1970 to 1986 171
8.4 Trends in SOE and public sector balances 172
