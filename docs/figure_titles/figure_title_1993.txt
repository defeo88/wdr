1 Demographic regions used in this Report 2
2 Burden of disease attributable to premature mortality and disability, by demographic region, 1990 3
3 Infant and adult mortality in poor and nonpoor neighborhoods of Porto Alegre, Brazil, 1980 7
1.1 Child mortality by country, 1960 and 1990 22
1.2 Trends in life expectancy by demographic region, 1950-90 23
1.3 Age-standardized female death rates in Chile and in England and Wales, selected years 24
1.4 Change in female age-specific mortality rates in Chile and in England and Wales, selected years 24
1.5 Disease burden by sex and demographic region, 1990 28
1.6 Distribution of disability-adjusted life years (DALYs) lost, by cause, for selected demographic regions, 1990 29
1.7 Trends in life expectancy and fertility in Sub-Saharan Africa and Latin America and the Caribbean, 1960-2020 30
1.8 Median age at death, by demographic region, 1950, 1990, and 2030 32
1.9 Life expectancy and income per capita for selected countries and periods 34
2.1 Mutually reinforcing cycles: reduction of poverty and development of human resources 37
2.2 Child mortality in rich and poor neighborhoods in selected metropolitan areas, late 1980s 40
2.3 Declines in child mortality and growth of income per capita in sixty-five countries 41
2.4 Effect of parents' schooling on the risk of death by age 2 in selected countries, late 1980s 43
2.5 Schooling and risk factors for adult health, Porto Alegre, Brazil, 1987 44
2.6 Deviation from mean levels of public spending on health in countries receiving and not receiving adjustment lending, 1980-90 46
2.7 Enrollment ratios in India, by grade, about 1980 47
3.1 Life expectancies and health expenditures in selected countries: deviations from estimates based on GDP and schooling 54
3.2 Benefits and costs of forty-seven health interventions 62
4.1 Child mortality (in specific age ranges) and weight-for-age in Bangladesh, India, Papua New Guinea, and Tanzania 77
4.2 Total fertility rates by demographic region, 1950-95 82
4.3 Risk of death by age 5 for fertility-related risk factors in selected countries, late 1980s 83
4.4 Maternal mortality in Romania, 1965-91 86
4.5 Trends in mortality from lung cancer and various other cancers among U.S. males, 1930-90 88
4.6 Population without sanitation or water supply services by demographic region, 1990 91
4.7 Simulated AIDS epidemic in a Sub-Saharan African country 100
4.8 Trends in new HIV infections under a1terntive assumptions, 1990-2000: Sub-Saharan Africa and Asia 101
5.1 Income and health spending in seventy countries, 1990 110
5.2 Public financing of health services in low- and middle-income countries, 1990 117
6.1 The health system pyramid: where care is provided 135
6.2 Hospital capacity by demographic region, about 1990 136
6.3 Supply of health personnel by demographic region, 1990 or most recent available year 140
7.1 Disbursements of external assistance for the health sector; 1990 166
