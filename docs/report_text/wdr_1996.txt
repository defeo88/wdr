Oxford University Press 

OXFORD NEW YORK TORONTO DELHI BOMBAY CALCUTTA 

MADRAS KARACHI KUALA LUMPUR SINGAPORE HONG KONG 

TOKYO NAIROBI DAR ES SALAAM CAPE TOWN MELBOURNE 
AUCKLAND 

and associated companies in 

BERLIN IBADAN 

O 1996 The International Bank for Reconstruction and 

Development / The World Bank 

1818 H Street, NW, Washington, D.C. 20433, U.SA. 

Published by Oxford University Press, Inc. 

200 Madison Avenue, New York, N.Y. 10016 

Oxford is a registered trademark of Oxford University Press. 

All rights reserved. No part of this publication may be reproduced, stored 
in a retrieval system, or transmitted, in any form or by any means, electronic, 
mechanical, photocopying, recording, or otherwise, without the 
prior permission of Oxford University Press. 

Manufactured in the United States of America 

First printing June 1996 

This volume is a product of the staff of the World Bank, and the 
judgments made herein do not necessarily reflect the views of its Board 

of Executive Directors or the countries they represent. The World Bank 

does not guarantee the accuracy of the data included in this publication 

and accepts no responsibility whatsoever for any consequence of their 

use. The boundaries, colors, denominations, and other information 
shown on any map in this volume do not imply on the part of the 
World Bank any judgment on the legal status of any territory 
or the endorsement or acceptance of such boundaries. 

ISBN 0-19-521108-1 clothbound 
ISBN 0-19-521107-3 paperback 
ISSN 0163-5085 

S. 
Text printed on recycled paper that conforms to the American Standard 
for Permanence of Paper for Printed Library Material Z39.48-1984 


Table 1. Classification of economies by income and region, 1996 

Sub-Saharan Africa Europe and Central Asia 
East and Asia Eastern Middle East and North Africa 
Income Southern East Asia Europe and Rest of Middle North 
group Subgroup Africa West Africa and Pacific South Asia Central Asia Europe East Africa Americas 
Burundi Benin Cambodia Afghanistan Albania Yemen, Rep. Egypt, Arab Guyana 
Comoros Burkina Faso China Bangladesh Armenia Rep. Haiti 
Eritrea Cameroon Lao PDR Bhutan Azerbaijan Honduras 
Ethiopia Central African Mongolia India Bosnia and Nicaragua 
Kenya Republic Myanmar Nepal Herzegovina 
Lesotho Chad Vietnam Pakistan Georgia 
Madagascar Congo Sri Lanka Kyrgyz 
Malawi Cote d'Ivoire Republic 
Mozambique Equatorial Tankistan 
Rwanda Guinea Republic 
Somalia Gambia, The 
Low-Sudan Ghana 
income Tanzania Guinea 
Uganda Guinea-Bissau 
Zaire Liberia 
Zambia Mali 
Zimbabwe Mauritania 
Niger 
Nigeria 
Sao Tome 
and 
Principe 
Senegal 
Sierra Leone 
Togo 
Angola Cape Verde Fiji Maldives Belarus Turkey Iran, Islamic Algeria Belize 
Botswana Indonesia Bulgaria Rep. Morocco Bolivia 
Djibouti Kiribati Croatia Iraq Tunisia Colombia 
Namibia Korea, Dem. Estonia Jordan Costa Rica 
Swaziland Rep. Kazakstan Lebanon Cuba 
Marshall Latvia Syrian Arab Dominica 
Islands Lithuania Republic Dominican 
Micronesia, Macedonia, West Bank Republic 
Fed. Sts. FYIV-and Gaza Ecuador 
N. Mariana Moldova El Salvador 
Lower Islands Poland Grenada 
Papua New Romania Guatemala 
Guinea Russian Jamaica 
Philippines Federation Panama 
Solomon Slovak Paraguay 
Islands Republic Peru 
Thailand Turkmenistan St. Vincent 
Tonga Ukraine and the 
Vanuatu Uzbekistan Grenadines 
Western Yugoslavia, Suriname 
Middle-Samoa Fed. Rep. Venezuela 
income Mauritius Gabon American Czech Greece Bahrain Libya Antigua and 
Mayotte Samoa Republic Isle of Man Oman Barbuda 
Reunion Guam Hungary Malta Saudi Arabia Argentina 
Seychelles Korea, Rep. Slovenia Barbados 
South Africa Malaysia Brazil 
New Chile 
Caledonia French 
Guiana 
Guadeloupe 
Upper Martinique 
Mexico 
Puerto Rico 
St. Kitts and 
Nevis 
St. Lucia 
Trinidad and 
Tobago 
Uruguay 
Subtotal: 165 27 23 25 8 27 4 10 5 36 


Table 1 (continued) 

Sub-Saharan Aftica Europe and Central Asia 
Asia Middle East and North Africa

East and Eastern 
Income Southern East Asia and Europe and Rest of Middle North 
group Subgroup Africa West Africa Pacific South Asia Central Asia Europe East Africa Americas 
Australia Austria Canada 
Japan Belgium United States 
New Zealand Denmark 
Finland 
France 
Germany 
Icdand 
Ireland 
OECD Italy 
countries Luxembourg 
Netherlands 
High-Norway 
income Portugal 
Spain 
Sweden 
Switzerland 
United 
Kingdom 
Brunei Andorra Israel Aruba 
French Channel Kuwait Bahamas, The 
Polynesia Islands Qatar Bermuda 
Non-OECD Hong Kong Cyprus United Arab Cayman 
countries Macao Faeroe Islands Emirates Islands 
Singapore Greenland Netherlands 
OAEb Liechtenstein Antilles 
Monaco Virgin Islands 
(U.S.) 
Total: 210 27 23 34 8 27 28 14 5 44 

Former Yugoslav Republic of Macedonia. 
Other Asian economiesTaiwan, China. 

For operational and analytical purposes, the World Definitions of groups 
Bank's main criterion for classifying economies is gross 

These tables classify all World Bank member countries

national product (GNP) per capita. Every economy is 

and all other economies with populations of more than

classified as low income, middle income (subdivided into 

30,000.

lower-middle and upper-middle), or high income. Other 
analytical groups, based on geographic regions, exports, 

Income group: Economies are divided according to 1994

and levels of external debt, are also used. 

GNP per capita, calculated using the World Bank Atlas 
method. The groups are: low income, $725 or less; lower


Low-income and middle-income economies are some-

middle income, $726 to $2,895; upper-middle income,

times referred to as developing economies. The use of the 

$2,896 to $8,955; and high income, $8,956 or more.

term is convenient; it is not intended to imply that all 
economies in the group are experiencing similar develop-

The estimates for the republics of the former Soviet

ment or that other economies have reached a preferred or 

Union are preliminary and their classification will be kept

final stage of development. Classification by income does 

under review.

not necessarily reflect development status. 


Table 2. Classification of economies by major export category and indebtedness, 1996 

Low- and middle-income 
Low-income Middle-income 
Severely Moderately Less Severely Moderately Less Not classified High-income 
Group indebted indebted indebted indebted indebted indebted by indebtedness OECD Non-OECD 
India Armenia Bulgaria Russian Belarus Canada Hong Kong 
Pakistan China Federation Czech Republic Finland Israel 
Georgia Estonia Germany Macao 
Kyrgyz Korea, Dem. Ireland Singapore 
Republic Rep. Italy OAP 
Korea, Rep. Japan 
Exporters of Latvia Sweden 
manufactures Lebanon Switzerland 
Lithuania 
Malaysia 
Moldova 
Romania 
Thailand 
Ukraine 
Uzbekistan 
Burundi Albania Mongolia Bolivia Chile Botswana American Iceland Faeroe Islands 
Cote d'Ivoire Chad Cuba Namibia Samoa New Zealand Greenland 
Equatorial Malawi Peru Solomon French Guiana 
Guinea Zimbabwe Islands Guadeloupe 
Ghana Suriname Reunion 
Guinea Swaziland 
Guinea-Bissau Islands 
Guyana 
Honduras 
Liberia 
Madagascar 
Mali 
Exporters Mauritania 
of nonfi4el Myanmar 
prima), Nicaragua 
products Niger 
Rwanda 
Sao Tome and 
Principe 
Somalia 
Sudan 
Tanzania 
Togo 
Uganda 
Vietnam 
Zaire 
Zambia 
Congo Algeria Venezuela Bahrain Brunei 
Nigeria Angola Iran, Islamic Qatar 
Gabon Republic United Arab 
Exporters Iraq Libya Emirates 
offliels Oman 
(mainly oil) Saudi Arabia 
Trinidad and 
Tobago 
Turkmenistan 
Cambodia Benin Bhutan Jamaica Cape Verde Antigua and Martinique United Aruba 
Ethiopia Comoros Burkina Faso Jordan Dominican Barbuda Kingdom Bahamas, The 
Mozambique Egypt, Arab Lesotho Panama Republic Barbados Bermuda 
Yemen, Rep. Rep. Greece Belize Cayman 
Gambia, The Morocco Djibouti Islands 
Haiti Western Samoa El Salvador Cyprus 
Nepal Fiji French 
Grenada Polynesia 
Exporters Kiribati Kuwait 
of services Maldives Monaco 
Paraguay 
Seychelles 
St. Kitts and 
Nevis 
St. Lucia 
Tonga 
Vanuatu 


Table 2 (continued) 

Severely 
Group indebted 
Afghanistan 
Cameroon 
Central African 
Republic 
Kenya 
Diversified Sierra Leone 
exporters" 

Not classified 
by export 
category 

Number of 
economies: 210 36 

Low- and middle-income 
Low-income 


Moderately Less Severely 

indebted indebted indebted 
Bangladesh Azerbaijan Argentina 
Lao PDR Sri Lanka Brazil 
Senegal Tajikistan Ecuador 

Mexico 
Poland 
Syrian Arab 

Republic 

15 11 17 

Middle-income 

Moderately Less 
indebted indebted 
Colombia Costa Rica 
Hungary Dominica 
Indonesia Guatemala 
Papua New Kazakstan 
Guinea Malta 
Philippines Mauritius 
Tunisia South Africa 
Turkey St. Vincent 
Uruguay and the 
Grenadines 
Yugoslavia, 
Fed. Rep. 
Croatia 
Macedonia, 
FYR.c 
New Caledonia 
Slovak 
Republic 
Slovenia 

16 55 

Not classified 
by indebtedness 

Bosnia and 

Herzegovina 
Eritrea 
Guam 
Isle of Man 
Marshall 

Islands 
Mayotte 
Micronesia, 

Fed. Sts. 

N. Mariana 
Islands 
Puerto Rico 
West Bank and 

Gaza 

15 

High-income 

OECD Non-OECD 
Australia Netherlands 
Austria Antilles 
Belgium 
Denmark 
France 
Luxembourg 
Netherlands 
Norway 
Portugal 
Spain 
United States 
Andorra 
Channel 
Islands 
Liechtenstein 
Virgin Islands 
(U.S.) 
22 23 

Other Asian economiesTaiwan, China. 

Economies in which no single export category accounts for 50 percent or more of total exports. 

Former Yugoslav Republic of Macedonia. 

Definitions of groups 

These tables classify all World Bank member economies 
plus all other economies with populations of more than 
30,000. 

Major export category: Major exports are those that 
account for 50 percent or more of total exports of goods 
and services from one category in the period 1990-93. 
The categories are: nonfuel primary (SITC 0, 1, 2, 4, plus 
68); fuels (SITC 3); manufactures (SITC 5 to 9, less 68); 
and services (factor and nonfactor service receipts plus 
workers' remittances). If no single category accounts for 
50 percent or more of total exports, the economy is classified 
as diversified. 

Indebtedness: Standard World Bank definitions of severe 
and moderate indebtedness, averaged over three years 
(1992-94), are used to classify economies in this table. 

Severely indebted means that either of the two key ratios is 
above critical levels: present value of debt service to GNP 
(80 percent) and present value of debt service to exports 
(220 percent). Moderately indebted means that either of 
the two key ratios exceeds 60 percent of, but does not 
reach, the critical levels. For economies that do not report 
detailed debt statistics to the World Bank Debtor Reporting 
System (DRS), present-value calculation is not possible. 
Instead, the following methodology is used to dassify 
the non-DRS economies. Severely indebted means that 
three of four key ratios (averaged over 1992-94) are above 
critical levels: debt to GNP (50 percent); debt to exports 
(275 percent); debt service to exports (30 percent); and 
interest to exports (20 percent). Moderately indebted means 
that three of the four key ratios exceed 60 percent of, but 
do not reach, the critical levels. All other classified low-
and middle-income economies are listed as less-indebted. 


World Development 
Report 1996: 
From Plan to Market 


Development Report 1996: From Plan to Market steps back from the extra-

World

ordinary array of recent events and policy changes in 28 former centrally planned 
economiesthose in Central and Eastern Europe and the newly independent states of the 
former Soviet Union, along with China, Mongolia and Vietnamto ask what we have 
learned about the key elements of any successful transition and how they should be 
pursued. 

Available June 28, 1996. Published for the World Bank by Oxford University Press. 

Full English text available in two formats: 

English (Hardback) Stock no. 61108 (ISBN 0-19-521108-1) $45.95. 
English (Paperback) Stock no. 61107 (ISBN 0-19-521107-3) $22.95. 


Forthcoming in paperback; $22.95 each: 

Arabic: Stock no. 13269 (ISBN 0-8213-3269-4) September 1996 
Chinese: Stock no. 13268 (ISBN 0-8213-3268-6) September 1996 
French: Stock no. 13264 (ISBN 0-8213-3264-3) August 1996 
German: Stock no. 13266 (ISBN 0-8213-3266-X) September 1996 
Japanese: Stock no. 13267 (ISBN 0-8213-3267-8) September 1996 
Portuguese: Stock no. 13271 (ISBN 0-8213-3271-6) September 1996 
Russian: Stock no. 13270 (ISBN 0-8213-3270-8) September 1996 
Spanish: Stock no. 13265 (ISBN 0-8213-3265-1) August 1996 


World Bank Publications 

t : 
I I 


Distributors of World Bank 
Publications 

Prices and credit terms vary from country 
to country. Consult your local distributor 
before placing an order. 

ALBANIA 
Adrion Ltd. 
Perlat Rexhepi Str. 
Pall. 9, Shk. 1, Ap. 4 
Tirana 

ARGENTINA 
Oficina del Libro Internacional 
Av. Cordoba 1877 
1120 BuenosAires 

AUSTRALIA, FIJI, PAPUA NEW 
GUINEA, SOLOMON ISLANDS, 
VANUATU, AND WESTERN SAMOA 

D.A. Information Services 
648 Whitehorse Road 
Mitcham 3132 
Victoria 
AUSTRIA 
Gerold and Co. 
Graben 31 
A-1011 Wien 

BANGLADESH 
Micro Industries DevelopmentAssistance 
Society (MIDAS) 
House 5, Road 16 
Dhanmondi R/Area 
Dhaka 1209 

BELGIUM 
Jean De Lannoy 
Av. du Roi 202 
1060 Brussels 

BRAZIL 
Publicacoes Tecnicas 
Intemacionais Ltda. 
Rua Peixoto Gomide, 209 
01409 Sao Paulo, SP. 

CANADA 
Renouf Publishing Co. Ltd. 
1 294 Algoma Road 
Ottawa 
Ontario KlB 3W8 

CHINA 
China Financial & Economic Publishing 
House 8, Da Fo Si Dong Jie, 

Beijing 

COLOMBIA 
Infoenlace Ltda. 
ApartadoAereo 34270 
Bogota D.E. 

COTE D'IVOIRE 
Centre d'Edition et de Diffusion Africaines 
(CEDA) 
04 B.P. 541 
Abidjan 04 Plateau 

CYPRUS 
Center of Applied Research 
Cyprus College 
6, Diogenes Street, Engomi 

P.O. Box 2006 
Nicosia 
CZECH REPUBLIC 
National Information Center 
prodejna, Konviktska 5 
CS 113 57 Prague 1 

DENMARK 

SamfundsLitteratur 

RosenoemsAlle 11 
DK-1970 Frederiksberg C 

EGYPT, ARAB REPUBLIC OF 
Al Ahram 
Al Galaa Street 
Cairo 

The Middle East Observer 
41, Sherif Street 
Cairo 

FINLAND 
Akateeminen Kirjakauppa 

P.O. Box 23 
FIN-00371 Helsinki 
FRANCE 
World Bank Publications 
66, avenue d'Iena 
75116 Paris 

GERMANY 
UNO-Veriag 
PoppelsdorferAllee 55 
53115 Bonn 

GREECE 
Papasotiriou S.A. 
35, Stournara Str.,106 82Athens 

HONG KONG, MACAO 
Asia 2000 Ltd. 
Sales & Circulation Department 
Seabird House, 
unit 1101-02 
22-28 Wyndham Street, Central, Hong 
Kong 

HUNGARY 
Foundation for Market Economy 
Dombovari Ut 17-19 
H-1117 Budapest 

INDIA 
Allied Publishers Ltd. 
751 Mount R 
Madras -600002 

INDONESIA 
Pt. Indira Limited 
Jalan Borobudur 20 

P.O. Box 181 
Jakarta 10320 
IRAN 
Kowkab Publishers 


P.O. Box 19575-511 Tehran 
Ketab Sara Co. Publishers 
Khaled Eslamboli Ave.,6th St. 
Kusheh Delafrooz No. 8 
Tehran 

IRELAND 
Government SuppliesAgency 
Oifig an tSolathair 
4-5 Harcourt Road 
Dublin 2 

ISRAEL 
Yozmot Literature Ltd. 


P.O. Box 56055 
Tel Aviv 61560 
R.O.Y. International 
PO Box 13056 
Tel Aviv 61130 
Palestinian Authority/Middle East 
Index Information Services 

P.O.B. 19502 Jerusalem 
ITALY 
Licosa Commissionaria Sansoni SPA 
Via Duca Di Calabria, 1/1 
Casella Postale 552 
50125 Firenze 

JAMAICA 
Ian Randle Publishers Ltd. 
206 Old Hope Road, Kingston 6 

JAPAN 
Eastern Book Service 
Hongo 3-Chome, 
Bunkyo-ku 113 
Tokyo 

KENYA 
Africa Book Service (E.A.) Ltd. 
Quaran House 

Mfangano Street 

P.O. Box 45245, Nairobi 
KOREA, REPUBLIC OF 
Daejon Trading Co. Ltd. 

P.O. Box 34 
Yeoeida, Seoul 

MALAYSIA 
University of Malaya 
CooperativeBookshop, Limited, P.O. Box 

Jalan Pantai Baru 
59700 Kuala Lumpur 

MEXICO 
INFOTEC 
Apartado Postal 22-860 
14060 Tlalpan 
Mexico D.F. 

NETHERLANDS 
De Lindeboom/InOr-Publilcaties 

P.O. Box 202 
7480AE Haaksbergen 
NEW ZEALAND 
EBSCO NZ Ltd. 
Private Mail Bag 99914 
New Market, Auckland 

NIGERIA 
University Press Limited 
Three Crowns Building Jericho 
Private Mail Bag 50 
lbadan 

NORWAY 
Narvesen Information Center 
Book Department 

P.O. Box 6125 Etterstad 
N-0602 Oslo 6 
PAKISTAN 
Mirza BookAgency 
65, Shahrah-e-Quaid-e-Azam 

P.O. Box No. 729 
Lahore 54000 
Oxford University Press 
5 Bangalore Town 
Sharae Faisal 
PO Box 13033 
Karachi-75350 

PERU 
Editorial Desarrollo SA 
Apartado 3824 
Lima 1 

PHILIPPINES 
International Booksource Center Inc.. 
Suite 720, Cityland 10 
Condominium Tower 2 

H.V dela Costa, corner 
Valero St., Makati, Metro Manila 
POLAND 
International Publishing Service 
Ul. Piekna 31/37 
00-577 Warzawa 

PORTUGAL 
Livrada Portugal 
Rua Do Carmo 70-74 
1200 Lisbon 

ROMANIA 
Compani De Librarii Bucuresti S.A. 
Str. Lipscani no. 26 
sector 3 
Bucharest 

RUSSIAN FEDERATION 
lsdatelstvo Wes Mir> 
9a, Kolpachniy Pereulok 
Moscow 101831 

SAUDI ARABIA, QATAR 
Jarir Book Store 

P.O. Box 3196 
Riyadh 11471 
SINGAPORE, TAIWAN, 
MYANMAR, BRUNEI 
Asahgate Publishing Asia 

Pacific Pte. Ltd. 
41 Kallang Pudding Road #04-03 
Golden Wheel Building 
Singapore 349316 

SLOVAK REPUBLIC 
Slovad G.T.G. Ltd. 
Krupinska 4 
PO Box 152 
852 99 Bratislava 5 

SOUTH AFRICA, BOTSWANA 
For single titles: 
Oxford University Press Southern Africa 

P.O. Box 1141 
Cape Town 8000 
For subscription orders: 
International Subscription Svc. 

P.O. Box 41095 
Craighall 
Johannesburg 2024 
SPAIN 
Mundi-Prensa Libros, S.A. 
Castello 37 
28001 Madrid 

Mundi-Prensa Barcelona 
Conseil de Cent, 391 
08009 Barcelona 

SRI LANKA 
THE MALDIVES 
Lake House Bookshop 

P.O. Box 244 
100, Sir Chittampalam A. 
Gardiner Mawatha 
Colombo 2 
SWEDEN 
Fritzes Customer Service 
Regeringsgaton 12 
S-106 47 
Stockholm 

Wennergren-WilliamsAB 

P. O. Box 1305 
S-171 25 
Solna 
SWITZERLAND 
Librairie Payot 
Service Institutionnel 
Cotes-de-Montbenon 30 
1002 Lausanne 

Van Diermen Editions Techniques 
Ch. de Lacuez 41 
CH1807 Blonay 

TANZANIA 
Oxford University Press 
Maktaba Street 
PO Box 5299 
Dar es Salaam 

THAILAND 
Central Books Distribution 
306 Silom Road 
Bangkok 

TRINIDAD & TOBAGO, JAMAICA 
Systematics Studies Unit 
#9 Watts Street 
Curepe 
Trinidad 
West Indies 

UGANDA 
Gustro Ltd. 
Madhvani Building 
PO Box 9997 
Plot 16/4 Jinja Rd. 
Kampala 

UNITED KINGDOM 
Microinfo Ltd. 

P.O. Box 3 
Alton, 
Hampshire GU34 2PG 
England 
ZAMBIA 
University Bookshop 
Great East Road Campus 

P.O. Box 32379 
Lusaka 
ZIMBABWE 
Longman Zimbabwe 
(Pte.) Ltd. 
Tourle Road,Ardbennie 

P.O. Box ST125 
Southerton 

